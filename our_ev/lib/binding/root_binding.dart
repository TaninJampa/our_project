import 'package:get/get.dart';
import 'package:our_ev/controller/app_controller.dart';

class RootBinding implements Bindings {
  @override
  void dependencies() {
    Get.put(AppController());
  }
}
