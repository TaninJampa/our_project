import 'package:get/get.dart';

class AppController extends GetxController {
  RxInt counter = 0.obs;

  void increment() {
  counter.value++;
}
}